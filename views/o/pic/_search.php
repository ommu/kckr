<?php
/**
 * Kckr Pics (kckr-pic)
 * @var $this PicController
 * @var $model KckrPic
 * @var $form CActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.co>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2016 Ommu Platform (www.ommu.co)
 * @created date 1 July 2016, 07:41 WIB
 * @link https://bitbucket.org/ommu/kckr
 *
 */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	<ul>
		<li>
			<?php echo $model->getAttributeLabel('pic_id'); ?><br/>
			<?php echo $form->textField($model,'pic_id'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('publish'); ?><br/>
			<?php echo $form->textField($model,'publish'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('default'); ?><br/>
			<?php echo $form->textField($model,'default'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('pic_name'); ?><br/>
			<?php echo $form->textField($model,'pic_name'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('pic_nip'); ?><br/>
			<?php echo $form->textField($model,'pic_nip'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('pic_position'); ?><br/>
			<?php echo $form->textField($model,'pic_position'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('pic_signature'); ?><br/>
			<?php echo $form->textField($model,'pic_signature'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('creation_date'); ?><br/>
			<?php echo $form->textField($model,'creation_date'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('creation_id'); ?><br/>
			<?php echo $form->textField($model,'creation_id'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('modified_date'); ?><br/>
			<?php echo $form->textField($model,'modified_date'); ?>
		</li>

		<li>
			<?php echo $model->getAttributeLabel('modified_id'); ?><br/>
			<?php echo $form->textField($model,'modified_id'); ?>
		</li>

		<li class="submit">
			<?php echo CHtml::submitButton(Yii::t('phrase', 'Search')); ?>
		</li>
	</ul>
<?php $this->endWidget(); ?>
